# Spastbot

This simple Telegram bot waits for messages containing specific strings and responds with a specified message.

## Prerequisites
This bot depends on the [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot), you can install all dependencies from the `requirements.txt` file.

## Config
You need to add your bot's API key and a URL with the regex to check into the config.py like this:

```python
api_key = "API_KEY"
regex_url = "https://gist.githubusercontent.com/schuppentier/66f65cf4d39bb2b4ecff8be435101cef/raw/"
```

## Running
Just launch the bot with `./spastbot` either on a shell or with your preferred init system.

